---
title: Shutting down CKI
description: How to communicate a planned or unplanned shutdown of kernel testing
---

Internally, the One Portal should be used to announce planned and unplanned shutdowns.
For external clients, email is used instead.

## Outage

Using the [Create Outage] page, fill in the following form:

```text
* Title: { Outage description }

* Type: Incident

* Services: CKI

* Affected Locations/Data Center: { Affected locations }

* Impacted Users: { Use the preset value }

* Performer: { The person in charge of following the outage }

* Hostnames: { If applicable, e.g. gitlab.com }

* Status: Ongoing

* Priority: Critical

* Description: Description of the outage.

* Impact: All automated kernel testing is impacted. Testing that was running
  when the outage started will have to be restarted and all new testing will be
  delayed until the outage is over.

* Ticket: { Link to the SNOW Ticket }
```

Use this form when CKI is experiencing an outage or a service CKI depends on undergoes
a maintenance that cannot be worked around.

Make sure to also add the outage to the [outage spreadsheet].

## Maintenance

For CKI maintenances, fill in the form in [Create Outage] with the following data:

```text
* Title: { Maintenance description }

* Type: Maintenance

* Services: CKI

* Affected Locations/Data Center: { Affected locations }

* Impacted Users: { Use the preset value }

* Performer: { The person in charge of following the maintenance }

* Hostnames: { If applicable, e.g. gitlab.com }

* Status: Scheduled

* Date and Time, Estimated time required: { Date of the outage, including time
  to check that everything is up and running. }

* Priority: Medium

* Description: { Description of the maintenance }

  FAQ:

  Q: What if a test is running for one of my patches when the triggers are
     disabled?
  A: All of the tests that are running when the triggers are disabled will
     be allowed to finish.

  Q: What if I submit patches or Brew builds after the triggers are disabled?
  A: Those patches will be tested as soon as Beaker and the CKI pipelines
     are back online. None of those patches will be lost.

  Q: How can I stay informed about what is happening with the shutdown?
  A: Follow along with updates in this thread or join #kernelci on Red Hat IRC.

  Q: I have more questions about how this shutdown will affect me.
  A: Email us at cki-project@redhat.com or join #kernelci on Red Hat IRC.

* Impact:

  2019-12-23 16:00 UTC: CKI team disables all pipeline triggers.
  2019-12-26 08:00 UTC: GitLab maintenance begins.
  2019-12-30 12:00 UTC: CKI pipelines back online and testing kernels.

```

## External announcement email

```text
To: stable@vger.kernel.org
Cc: cki-project@redhat.com
Subject: CKI Project Shutdown: 2019-12-23 to 2019-12-30

Hello there,

The CKI team is planning to shutdown the kernel testing pipelines
including stable kernels during the holidays.

Shutdown timeline:

    2019-12-23 16:00 CET: CKI kernel testing pipelines are disabled.
    2019-12-30 12:00 CET: CKI kernel testing pipelines back online and testing.

FAQ:

Q: What if a test is running for one of my commits when the pipelines
   are disabled?
A: All of the tests that are running when the pipelines are disabled will
   be allowed to finish.

Q: What if I commit patches to one of the tested kernel trees after the
   pipelines are disabled?
A: The tip of those kernel trees will be tested as soon as the pipelines
   are back online.

Q: I have more questions about how this shutdown will affect me.
A: Email us at cki-project@redhat.com.

Thank you! Michael Hofmann and the CKI Project Team 🤖
```

[Create Outage]: https://one.redhat.com/outages/create
[outage spreadsheet]: https://docs.google.com/spreadsheets/d/1jeFI_AY56JkAQ6MRhGE0h5ckQU47ew4UJPQ76il3N-g/edit
