---
title: "Inventory"
description: A description of the different pieces that make up the CKI project
weight: 40
---

The CKI kernel testing setup consists of various pieces that roughly fit into
one of the following categories: code running in the pipeline, microservices,
and cron jobs. Additionally, this inventory lists storage and infrastructure
providers, external dependencies and the people in charge of maintaining all
those pieces.
