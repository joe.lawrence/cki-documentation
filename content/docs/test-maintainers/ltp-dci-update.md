---
title: LTP update procedure for internal builds
---

We update LTP for internal builds every three months to align with the
frequency of [LTP release][01]. To update it, we internally follow these steps:

* Create a branch on your fork of [kernel-tests][02] (_create a fork if you
  don't have one already_).
* Copy RHELKT1LITE.YYYYMMDD file (e.g. RHELKT1LITE.20210524) from
  [internal configs][03] to dir `distribution/ltp/lite/configs`.
* Update file `distribution/ltp/lite/runtest.sh` to use RHELKT1LITE.YYYYMMDD.
* Update `dci/*.xml` to use `YYYYMMDD`, e.g.

  ```xml
  <dependency url="https://github.com/linux-test-project/ltp/releases/download/20210524/ltp-full-20210524.tar.bz2"/>
  ```

* Push the branch with your changes with comment
  `Update LTP Stable to YYYYMMDD`.
* File an MR to repo [kernel-tests][02].
* Test your branch via `cki-ci-bot` in your MR.

[01]: https://github.com/linux-test-project/ltp/releases
[02]: https://gitlab.com/cki-project/kernel-tests
[03]: https://gitlab.cee.redhat.com/kernel-qe/kernel/-/blob/master/distribution/ltp/lite/configs
